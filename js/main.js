$(function () {
		  $('[data-toggle="tooltip"]').tooltip()
		  $('.carousel').carousel({
			  interval: 4000
			});

		  //Eventos jquery
		  $('#modal_contacto').on('show.bs.modal', function (e) {
		  		console.log('Se muestra el modal');
		  		$('#contactoBtn').removeClass('btn-primary');
		  		$('#contactoBtn').addClass('btn-secondary');
		  		$('#contactoBtn').prop('disable',true);
		  });
		  $('#modal_contacto').on('hidden.bs.modal', function (e) {
		  		console.log('Se ocultó el modal');
		  		$('#contactoBtn').removeClass('btn-secondary');
		  		$('#contactoBtn').addClass('btn-primary');
		  		$('#contactoBtn').prop('disable',false);
		  });
		  $('#modal_contacto').on('shown.bs.modal', function (e) {
		  		console.log('Se muestró el modal');

		  });
		  $('#modal_contacto').on('hide.bs.modal', function (e) {
		  		console.log('Se ocomienza a ocultar el modal');

		  });
});
